// npm -v
// node -v

// Clients sends a request for the server that in turn would query and manipulate the database
// Request comes from the client. Response comes from the server
// Node is a runtime environment used to build server-side application

// require function is a directive/function used to load a particular a node module
// In this case, we are trying to load the http module from node.js
// HTTP module - lets Node JS transfer data; a set of individual files needed to create a component
// Such component is used to establish data transfer between application
let http = require("http")

// createServer method creates HTTP server that listens to request on a specific port and gives the response back the client
// Accepts a function that allows performing of a task for the server
http.createServer((request, response) => {
    response.writeHead(200, {"Content-Type": "text/plain"})
    response.end("Hello World")
}).listen(4000)

console.log("Your server is now running at port:4000")

// response.writeHead response is used to set a status code for the response
// 200 status code is the default code for the Node JS since it means that the response is successfully processed

// response.end signals the end of the response
// listen() assigns the server to the specified port
// port is a virtual port where network connections start and end
// Each port is specific to a certain process/server 
// Content-Type sets the type of content which will be displayed on the client
// http://localhost:4000/
// node <file-name> runs the server
// ctrl + c terminates the server
// npx kill-port 4000 is an alternative command for ctrl + c