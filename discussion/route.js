const http = require('http')
const port = 4000

const server = http.createServer((request, response) => {
    if (request.url === "/greeting") {
        response.writeHead(200, {"Content-Type": "text/plain"})
        response.end("Hello World")
    }  
    else if (request.url === "/home") {
        response.writeHead(200, {"Content-Type": "text/plain"})
        response.end("Welcome to Home") 
    }
    else if (request.url === "/class") {
        response.writeHead(200, {"Content-Type": "text/plain"})
        response.end("Welcome to Class")
    }
    else {
        response.writeHead(404, {"Content-Type": "text/plain"})
        response.end("Page not found")
    }
})

server.listen(port)